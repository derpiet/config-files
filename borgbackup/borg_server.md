#### Quelle
https://www.youtube.com/watch?v=7ZgjElyUqZ4&t=903s&ab_channel=LinuxGuidesDE

## Setup

- User erstellen
```
su -
adduser serverbackup 
```
- SSH Key vom Quellserver auf dem Backup Server hinterlegen

## Borg initialisieren
```
borg init
```

## Backup Script erstellen
```
#!/bin/bash
# systemctl stop nginx
# sleep 20

DATE=`date +"%Y-%m-%d"`
REPOSITORY="ssh://serverbackup@168.119.115.116/~/backups/rauhe-berge"
export BORG_PASSPHRASE="<PASSWORD>"
borg create $REPOSITORY::$DATE /opt/traefik/core --exclude-caches
```

## Backup überprüfen

```
cd
mkdir mnt
borg mount backups/<borg_backup> mnt/
ls -la mnt/
```

## Cronjob einrichten
Am Beispiel jede Nacht um 2h
```
0 2 * * * </pfad/zum/script>
```

##

### BACKUPSERVER

```
borg umount mnt/
```

## Prune Script erstellen

```
#!/bin/bash

# Server Rauhe Berge:
export BORG_PASSPHRASE="<PASSWORD>"
borg prune -v ~/backups/rauhe-berge \
        --keep-daily=10 \
        --keep-weekly=4 \
        --keep-monthly=12

# Weiterer Server:
siehe oben
```

## Cronjob einrichten
Am Beispiel jede Nacht um 5h
```
0 2 * * * </pfad/zum/script>
```