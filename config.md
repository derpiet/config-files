# Ordner-/Dateiberechtigungen für nginx setzen

User & Gruppe auf www-data setzen. Ggfs nginx (User muss aber entsprechend angelegt werden)

## PHP inkl. benötigter Module installieren

	* sudo apt install php7.4-{fpm,gd,json,mbstring,mysql,xml,xmlrpc,opcache,cli,zip,soap,intl,bcmath,curl} php-ssh2
	* sudo apt install php8.0-{fpm,gd,mbstring,mysql,xml,opcache,cli,zip,soap,intl,bcmath,curl} php-ssh2

## PHP konfigurieren
	In /etc/php/7.4/fpm/php.ini folgender Werte anpassen:
	* upload_max_filesize = 100M
	* post_max_size = 100M
	* max_input_vars = 3000
	* memory_limit = 256M
	* allow_url_fopen = Off
	* cgi.fix_pathinfo=0

## OPCache
	* opcache.enable=1
	* opcache.memory_consumption=192
	* opcache.interned_strings_buffer=16
	* opcache.max_accelerated_files=7963 (Muss eine prime number sein, siehe dcode link)
		(find . -type f -print | grep php | wc -l)
	* opcache.validate_timestamps=0
	
	### Weiteres
	WordPress Plugin: Flush OpCache Plugin
	https://www.dcode.fr/closest-prime-number
